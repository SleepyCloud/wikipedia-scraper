from bs4 import BeautifulSoup
import requests
import os
from collections import deque

################## CONFIG #######################################

base = "http://en.wikipedia.org/wiki/" #base url to use
first = "Encryption" #variable to configure things a bit nicer

folder_to_write = 'pages' #the folder to dump the pages to

visited = [] #track the pages we've been to so not to loop
to_visit = deque([first]) #track the pages we've yet to visit

dataset_size = 300 # the size of the dataset to scrape
current_size = 0.0 #want a double

#################################################################

def write_to_file(filename, page):
    ''' 
    takes the name of the file and content to write and encodes the 
    page as ascii
    '''
    page = page.encode('ascii', 'ignore')
    with open(folder_to_write + '/' + filename + '.txt','w') as f:
        f.write(page)

def is_wiki(title):
    title = title.get('href')
    if 'Main_Page' in title or '?' in title or '#' in title or ':' in title or 'http' in title or 'www' in title or '//' in title: # not an in page anchor
        return False
    if title[6:] not in visited and '/' not in title[6:]:
        return True
    return False

if not os.path.exists(folder_to_write): #check that we have a dir to write to
    try:
        os.makedirs(folder_to_write) #make it if we don't
    except: # problem creating the directory
        print 'Error creating directory. Exiting...'
        exit()

print 'Writing to %s' % (folder_to_write)
print 'Starting on page %s' % (base + first)

while len(to_visit) > 0 and current_size < dataset_size: #while we have pages to get

    current = to_visit.popleft()
    print "Currently pulling %s" % (current)

    page = requests.get(base + current) #get the html for the page
    soup = BeautifulSoup(page.text) #parse it
    content = soup.select('#mw-content-text')[0].get_text() # get the text from the main content

    write_to_file(current, content) # write the page to a file

    # get  the size of the file in bytes and convert it to megabytes to increment the running total
    current_size += ((os.path.getsize(folder_to_write + '/' + current + '.txt') *1.0) /1024) /1024

    link_tags = soup.find_all('a')[1:] # get all of the links from the page
    links = [] #we want to hold the page name, not the URLs

    link_tags = filter(is_wiki, link_tags) # remove any links we don't want

    for link in link_tags: 
        links.append(link.get('href')[6:])#add the name of the page(minus /wiki/)

    to_visit.extend(links) #add the links we do want to our list to visit
    visited.append(current) #add this page so we don't  do it again

print 'Finished writing %d pages' % (len(visited))
